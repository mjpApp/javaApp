/**
 * Created by Administrator on 2016/12/22.
 */

    //datax在myajaxs文件中
var current= 1;
var accountCon={
    //向右翻页
    setright:function(){
        if(datax.datas.length===15){
            current=current+1;
            accountCon.getPage(current);
        }
        else{
            console.log(datax.datas.length);
            layer.msg("已经无法向右翻了");
        }
    },
    //向左翻页
    setleft:function(){
        if(current!==1){
            current=current-1;
            accountCon.getPage(current);
        }
        else{
            layer.msg("已经无法向左翻了");
        }
    },
    //得到分页
    getPage: function () {
       return  account_list.getPage(current);
    },
    //添加
    add: function () {
        var model=$('#accountModal input');
        var date =new Date();
        var time=date.getFullYear()+""+(date.getMonth()+1)+""+date.getDate()+""+date.getHours()+""+date.getMinutes()+""+date.getSeconds();
        console.log(time,"acc"+time);
        var data = {
            acc_id:"acc"+time,
            cus_id:model[1].value,
            acc_bankNum:model[2].value,
            acc_openBank:model[3].value,
            acc_subBank:model[4].value,
            acc_readyAccount:model[5].value,
            acc_costAccount:model[6].value,
            acc_monthAccount:model[7].value
        }
        if(data.cus_id&&data.acc_bankNum&&data.acc_openBank&&data.acc_subBank&&data.acc_readyAccount&&data.acc_costAccount&&data.acc_monthAccount){
            console.log(data);
            account_list.addOne(data);
        }else{
            layer.msg("填写不能为空");
        }
    },
    //修改
    update:function(){
        var model=$('#accountModal input');
        var data = {
            acc_id:model[0].value,
            cus_id:model[1].value,
            acc_bankNum:model[2].value,
            acc_openBank:model[3].value,
            acc_subBank:model[4].value,
            acc_readyAccount:model[5].value,
            acc_costAccount:model[6].value,
            acc_monthAccount:model[7].value
        }
        if(data.cus_id&&data.acc_bankNum&&data.acc_openBank&&data.acc_subBank&&data.acc_readyAccount&&data.acc_costAccount&&data.acc_monthAccount){
            console.log(data);
            account_list.upOne(data);
        }else{
            layer.msg("填写不能为空");
        }
    }
    ,
    getOne:function(){
        var model=$('#accountModal input');
        var acc_id=model[0].value;
        account_list.getOne(model,acc_id);
    },
    removes:function(){
        var model=$('#accountModal input');
        var acc_id=model[0].value;
        console.log(acc_id);
        account_list.removeOne(acc_id);
    }
};

var customerCon={
    //向右翻页
    setright:function(){
        if(datax.datas.length===15){
            current=current+1;
            customerCon.getPage(current);
        }
        else{
            console.log(datax.datas.length);
            layer.msg("已经无法向右翻了");
        }
    },
    //向左翻页
    setleft:function(){
        if(current!==1){
            current=current-1;
            customerCon.getPage(current);
        }
        else{
            layer.msg("已经无法向左翻了");
        }
    },
    //得到分页
    getPage: function () {
        return  customer_list.getPage(current);
    },
    //添加
    add: function () {
        var model=$('#customerModal input');
        var date =new Date();
        var time=date.getFullYear()+""+(date.getMonth()+1)+""+date.getDate()+""+date.getHours()+""+date.getMinutes()+""+date.getSeconds();
        console.log(time,"acc"+time);
        var data = {
            cus_id:"cus"+time,
            cus_name:model[1].value,
            pre_type:model[2].value,
            cus_community:model[3].value,
            cus_manager:model[4].value,
            cus_industry:model[5].value,
            cus_commissioner:model[6].value
        }
        if(data.cus_name&&data.pre_type&&
            data.cus_community&&data.cus_manager&&
            data.cus_industry&&
            data.cus_commissioner){
            console.log(data);
            customer_list.addOne(data);
        }else{
            layer.msg("填写不能为空");
        }
    },
    //修改
    update:function(){
        var model=$('#customerModal input');
        var data = {
            cus_id:model[0].value,
            cus_name:model[1].value,
            pre_type:model[2].value,
            cus_community:model[3].value,
            cus_manager:model[4].value,
            cus_industry:model[5].value,
            cus_commissioner:model[6].value
        }
        if(data.cus_name&&data.pre_type&&
                data.cus_community&&data.cus_manager&&
                data.cus_industry&&
                data.cus_commissioner){
            console.log(data);
            customer_list.upOne(data);
        }else{
            layer.msg("填写不能为空");
        }
    }
    ,
    getOne:function(){
        var model=$('#customerModal input');
        var acc_id=model[0].value;
        customer_list.getOne(model,acc_id);
    },
    removes:function(){
        var model=$('#customerModal input');
        var cus_id=model[0].value;
        console.log(cus_id);
        customer_list.removeOne(cus_id);
    }
};

var userCon={
    //向右翻页
    setright:function(){
        if(datax.datas.length===15){
            current=current+1;
            userCon.getPage(current);
        }
        else{
            console.log(datax.datas.length);
            layer.msg("已经无法向右翻了");
        }
    },
    //向左翻页
    setleft:function(){
        if(current!==1){
            current=current-1;
            userCon.getPage(current);
        }
        else{
            layer.msg("已经无法向左翻了");
        }
    },
    //得到分页
    getPage: function () {
        return  user_list.getPage(current);
    },
    //添加
    add: function () {
        var model=$('#userModal input');
        var date =new Date();
        var time=(date.getFullYear()-2000)+""+(date.getMonth()+1)+""+date.getDate()+""+date.getHours()+""+date.getMinutes()+""+date.getSeconds();
        //console.log(time.slice(0,11),"phone"+time,model[1].value,model[2].value);
        var data = {
            user_phone:time.slice(0,11),
            user_address:model[1].value,
            acc_bankNum:model[2].value
        }
        if(data.user_address&&data.acc_bankNum){
            console.log(data);
            user_list.addOne(data);
        }else{
            layer.msg("填写不能为空");
        }
    },
    //修改
    update:function(){
        var model=$('#userModal input');
        var data = {
            user_phone:model[0].value,
            user_address:model[1].value,
            acc_bankNum:model[2].value
        }
        if(data.user_address&&data.acc_bankNum){
            console.log(data);
            user_list.upOne(data);
        }else{
            layer.msg("填写不能为空");
        }
    }
    ,
    getOne:function(){
        var model=$('#userModal input');
        var user_phone=model[0].value;
        user_list.getOne(model,user_phone);
    },
    removes:function(){
        var model=$('#userModal input');
        var user_phone=model[0].value;
        console.log(user_phone);
        user_list.removeOne(user_phone);
    }
};

var billCon= {
    //向右翻页
    setright: function () {
        if (datax.datas.length === 15) {
            current = current + 1;
            billCon.getPage(current);
        }
        else {
            console.log(datax.datas.length);
            layer.msg("已经无法向右翻了");
        }
    },
    //向左翻页
    setleft: function () {
        if (current !== 1) {
            current = current - 1;
            billCon.getPage(current);
        }
        else {
            layer.msg("已经无法向左翻了");
        }
    },
    //得到分页
    getPage: function () {
        return bill_list.getPage(current);
    }
}

var workCon = {
    getOne:function(){
        var model=$('#myModal input');
        var worker_id=model[0].value;
        worker_list.getOne(model,worker_id);
    },
    //修改
    update:function(){
        var model=$('#myModal input');
        var data = {
            worker_id:model[0].value,
            worker_account:model[1].value,
            worker_password:model[2].value,
            worker_name:model[3].value,
            worker_sex:model[5].value,
            worker_age:model[4].value,
            post_id:model[6].value
        }

        if(data.post_id&&data.worker_account&&data.worker_password&&data.worker_name&&data.worker_sex&&data.worker_age){
            console.log(data);
            worker_list.upOne(data);
        }else{
            layer.msg("填写不能为空");
        }
    }
}