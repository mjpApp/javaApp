/**
 * Created by Administrator on 2016/12/22.
 */
var current= 1;
var customerCon={
    //向右翻页
    setright:function(){
        if(datax.datas.length===15){
            current=current+1;
            customerCon.getPage(current);
        }
        else{
            console.log(datax.datas.length);
            layer.msg("已经无法向右翻了");
        }
    },
    //向左翻页
    setleft:function(){
        if(current!==1){
            current=current-1;
            customerCon.getPage(current);
        }
        else{
            layer.msg("已经无法向左翻了");
        }
    },
    //得到分页
    getPage: function () {
        return  customer_list.getPage(current);
    },
    //添加
    add: function () {
        var model=$('#customerModal input');
        var date =new Date();
        var time=date.getFullYear()+""+(date.getMonth()+1)+""+date.getDate()+""+date.getHours()+""+date.getMinutes()+""+date.getSeconds();
        console.log(time,"acc"+time);
        var data = {
            acc_id:"acc"+time,
            cus_id:model[1].value,
            acc_bankNum:model[2].value,
            acc_openBank:model[3].value,
            acc_subBank:model[4].value,
            acc_readycustomer:model[5].value,
            acc_costcustomer:model[6].value,
            acc_monthcustomer:model[7].value
        }
        if(data.cus_id&&data.acc_bankNum&&data.acc_openBank&&data.acc_subBank&&data.acc_readycustomer&&data.acc_costcustomer&&data.acc_monthcustomer){
            console.log(data);
            customer_list.addOne(data);
        }else{
            layer.msg("填写不能为空");
        }
    },
    //修改
    update:function(){
        var model=$('#customerModal input');
        var data = {
            acc_id:model[0].value,
            cus_id:model[1].value,
            acc_bankNum:model[2].value,
            acc_openBank:model[3].value,
            acc_subBank:model[4].value,
            acc_readycustomer:model[5].value,
            acc_costcustomer:model[6].value,
            acc_monthcustomer:model[7].value
        }
        if(data.cus_id&&data.acc_bankNum&&data.acc_openBank&&data.acc_subBank&&data.acc_readycustomer&&data.acc_costcustomer&&data.acc_monthcustomer){
            console.log(data);
            customer_list.upOne(data);
        }else{
            layer.msg("填写不能为空");
        }
    }
    ,
    getOne:function(){
        var model=$('#customerModal input');
        var acc_id=model[0].value;
        customer_list.getOne(model,acc_id);
    },
    removes:function(){
        var model=$('#customerModal input');
        var acc_id=model[0].value;
        console.log(acc_id);
        customer_list.removeOne(acc_id);
    }
};