/**
 * Created by Administrator on 2016/12/22.
 */
var app = angular.module('app', ['ui.router'])
    .config(function ($stateProvider, $urlRouterProvider) {
        $urlRouterProvider.when("", "/");//����д.��
        $stateProvider
            .state("/", {
                url: "/",
                templateUrl: "view/worker_customer.html"
            })
            .state("account", {
                url: "/account",
                templateUrl: "view/worker_account.html"
            })
            .state("customer", {
                url: "/customer",
                templateUrl: "view/worker_customer.html"
            })
            .state("user", {
                url: "/user",
                templateUrl: "view/worker_user.html"
            })
            .state("bill", {
                url: "/bill",
                templateUrl: "view/worker_bill.html"
            })
            .state("banner", {
                url: "/banner",
                templateUrl: "view/banner.html"
            })
            .state("main", {
                url: "/main",
                templateUrl: "view/worker_main.html"
            })
        ;
    })