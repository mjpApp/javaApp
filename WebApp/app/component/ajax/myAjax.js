/**
 * Created by Administrator on 2016/12/17.
 */

function myAjax(){
}
//urls:string datas:Object
myAjax.prototype.post=function(urls,datas,colback) {
    $.ajax({
        type:"post",
        url:urls,
        data:datas,
        success:function(data){
            return colback(data);
        },
        error:function(data){
            layer.closeAll('loading');
            layer.msg('系统错误或者该账户已经被使用');
            return null;
        }
    });
    return null;
}
myAjax.prototype.view=function(data){
        datax.setdata(data);
        if(data.length!==0){
            $('#charge_table').datagrid("loadData",data);//将数据绑定到DataGrid中
        }
        return data;
}
myAjax.prototype.one=function(data){
        datax.setone(data);
        console.log(datax.getone())
        return data;
}
//原型模式
var myAjaxs=new myAjax();
console.log(myAjaxs);

function datay(){
    this.datas=null;
    this.one=null;
}
datay.prototype.setdata=function(data){
    this.datas=data;
}
datay.prototype.getdata=function(){
    return this.datas;
}
datay.prototype.setone=function(one){
    this.one=one;
}
datay.prototype.getone=function(){
    return this.one;
}
var datax = new datay();
console.log(datax);