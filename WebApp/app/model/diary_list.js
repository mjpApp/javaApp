/**
 * Created by penny on 2016/12/24.
 */
var diary_list=diary_list||{
        dia_id:'',
        worker_id:'',
        dia_type:'',
        dia_time:'',
        dia_object:'',
        dia_text:'',
        addOne:function(){
            myAjaxs.post("../../../diary/insOne",
                {
                    dia_id:diary_list.dia_id,
                    worker_id:diary_list.worker_id,
                    dia_type:diary_list.dia_type,
                    dia_time:diary_list.dia_time,
                    dia_object:diary_list.dia_object,
                    dia_text:diary_list.dia_text
                },function(data){
                    layer.msg("diary.addOne");
                    return data;
                });
        },
        removeOne:function(){
            myAjaxs.post("../../../diary/delOne",
                {
                    dia_id:diary_list.dia_id
                },function(data){
                    layer.msg("diary.removeOne");
                    return data;
                });
        },
        getOne:function(dia_id){
            myAjaxs.post("../../../diary/getOne",
                {
                    dia_id:dia_id
                },function(data){
                    layer.msg("diary.getOne");
                    return data;
                });
        },
        getAll:function(){
            myAjaxs.post("../../../diary/list",
                {

                },function(data){
                    layer.msg("diary.getAll");
                    return data;
                });
        },
        getPage:function(){
            myAjaxs.post("../../../diary/listOne",
                {
                    current:1,
                    number:10
                },function(data){
                    layer.msg("diary.getPage");
                    return data;
                });
        },
        upOne:function(){
            myAjaxs.post("../../../diary/upOne",
                {
                    dia_id:diary_list.dia_id,
                    worker_id:diary_list.worker_id,
                    dia_type:diary_list.dia_type,
                    dia_time:diary_list.dia_time,
                    dia_object:diary_list.dia_object,
                    dia_text:diary_list.dia_text
                },function(data){
                    layer.msg("diary.upOne");
                    return data;
                });
        }
    }