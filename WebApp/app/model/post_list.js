/**
 * Created by penny on 2016/12/24.
 */
var post_list=post_list||{
        post_id:'',
        post_wtype:'',
        pre_ktype:'',
        addOne:function(){
            myAjaxs.post("../../../post/insOne",
                {
                    post_id:post_list.post_id,
                    post_wtype:post_list.post_wtype,
                    pre_ktype:post_list.pre_ktype
                },function(data){
                    layer.msg("post.addOne");
                    return data;
                });
        },
        removeOne:function(){
            myAjaxs.post("../../../post/delOne",
                {
                    post_id:post_list.post_id
                },function(data){
                    layer.msg("post.removeOne");
                    return data;
                });
        },
        getOne:function(post_id){
            myAjaxs.post("../../../post/getOne",
                {
                    post_id:post_id,
                },function(data){
                    layer.msg("post.getOne");
                    return data;
                });
        },
        getAll:function(){
            myAjaxs.post("../../../post/list",
                {

                },function(data){
                    layer.msg("post.getAll");
                    return data;
                });
        },
        getPage:function(){
            myAjaxs.post("../../../post/listOne",
                {
                    current:1,
                    number:10
                },function(data){
                    layer.msg("post.getPage");
                    return data;
                });
        },
        upOne:function(){
            myAjaxs.post("../../../post/upOne",
                {
                    post_id:post_list.post_id,
                    post_wtype:post_list.post_wtype,
                    pre_ktype:post_list.pre_ktype
                },function(data){
                    layer.msg("post.upOne");
                    return data;
                });
        }
    }