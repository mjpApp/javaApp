/**
 * Created by Administrator on 2016/12/23.
 */
var user_list=user_list||{

    user_phone:"",
    user_address:"",
    acc_bankNum:"",

    login:function(user){
        myAjaxs.post("../../../user/getOne",
            {
                user_phone:user.user_phone
            },function(data){
                if(data.length==0){
                    layer.msg("不存在该号码,请重新输入");
                }
                else{
                    if(data.acc_bankNum==user.acc_bankNum){
                        sessionStorage.setItem('accusername',user.user_phone);
                        sessionStorage.setItem('accpassword',user.acc_bankNum);

                        window.location.href='../user/user.html';
                    }
                    else{
                        layer.msg("登录成功");
                    }
                }


                if(bankNum){
                    bankNum=data.acc_bankNum;
                }
                return data;
            });
    },
    addOne:function(data){
        myAjaxs.post("../../../user/insOne",
            data,function(data){
                if(data==1){
                    layer.msg("保存成功")
                }else{
                    layer.msg("保存失败")
                }
                return data;
        });
    },
    removeOne:function(user_phone){
        myAjaxs.post("../../../user/delOne",
            {
                user_phone:user_phone
            },function(data){
                if(data==1){
                    layer.msg("删除成功")
                }else{
                    layer.msg("删除失败")
                }
                return data;
            });
    },
    getOne:function(model,user_phone){
            myAjaxs.post("../../../user/getOne",
                {
                    user_phone:user_phone
                },function(data){
                    if(data.length==0){
                        layer.msg("不存在该号码,请重新输入");
                    }
                    else{
                        model[1].value=data.user_address;
                        model[2].value=data.acc_bankNum;
                    }
                    return data;
                });
        },
        getAll:function(){
            myAjaxs.post("../../../user/list",
                {

                },function(data){
                    layer.msg("user.getAll");
                    return data;
                });
        },
        getPage:function(current){
            layer.load(2);
            myAjaxs.post("../../../user/listOne",
                {
                    current:current,
                    number:15
                },function(data){
                    layer.closeAll('loading');
                    myAjaxs.view(data);
                    return data;
                });
        },
        upOne:function(data){
            myAjaxs.post("../../../user/upOne",
                {
                    user_phone:data.user_phone,
                    user_address:data.user_address,
                    acc_bankNum:data.acc_bankNum
                },function(data){
                    if(data==1){
                        layer.msg("更新成功")
                    }else{
                        layer.msg("更新失败")
                    }
                    return data;
                });
        }
}
