/**
 * Created by penny on 2016/12/24.
 */
var prefer_list=prefer_list||{
        pre_type:'',
        pre_percent:'',
        addOne:function(){
            myAjaxs.post("../../../prefer/insOne",
                {
                    pre_type:prefer_list.pre_type,
                    pre_percent:prefer_list.pre_percent
                },function(data){
                    layer.msg("prefer.addOne");
                    return data;
                });
        },
        removeOne:function(){
            myAjaxs.post("../../../prefer/delOne",
                {
                    pre_type:prefer_list.pre_type
                },function(data){
                    layer.msg("prefer.removeOne");
                    return data;
                });
        },
        getOne:function(pre_type){
            myAjaxs.post("../../../prefer/getOne",
                {
                    pre_type:pre_type
                },function(data){
                    layer.msg("prefer.getOne");
                    return data;
                });
        },
        getAll:function(){
            myAjaxs.post("../../../prefer/list",
                {

                },function(data){
                    layer.msg("prefer.getAll");
                    return data;
                });
        },
        getPage:function(){
            myAjaxs.post("../../../prefer/listOne",
                {
                    current:1,
                    number:10
                },function(data){
                    layer.msg("prefer.getPage");
                    return data;
                });
        },
        upOne:function(){
            myAjaxs.post("../../../prefer/upOne",
                {
                    pre_type:prefer_list.pre_type,
                    pre_percent:prefer_list.pre_percent
                },function(data){
                    layer.msg("prefer.upOne");
                    return data;
                });
        }
    }