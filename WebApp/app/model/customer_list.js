/**
 * Created by penny on 2016/12/24.
 */
var customer_list=customer_list||{
        cus_id:'',
        cus_name:'',
        pre_type:'',
        cus_community:'',
        cus_manager:'',
        cus_industry:'',
        cus_commissioner:'',
        addOne:function(temp){
            myAjaxs.post("../../../customer/insOne",
                temp,function(data){
                    if(data==1){
                        layer.msg("保存成功")
                    }else{
                        layer.msg("保存失败")
                    }
                    return data;
                });
        },
        removeOne:function(cus_id){
            myAjaxs.post("../../../customer/delOne",
                {
                    cus_id:cus_id
                },function(data){
                    if(data==1){
                        layer.msg("删除成功")
                    }else{
                        layer.msg("删除失败")
                    }
                    return data;
                });
        },
        getOne:function(model,cus_id){
            myAjaxs.post("../../../customer/getOne",
                {
                    cus_id:cus_id
                },function(data){
                    //layer.msg("customer.getOne");
                    if(data.length==0){
                        layer.msg("不存在该客户");
                    }
                    else{
                        model[1].value=data.cus_name;
                        model[2].value=data.pre_type;
                        model[3].value=data.cus_community;
                        model[4].value=data.cus_manager;
                        model[5].value=data.cus_industry;
                        model[6].value=data.cus_commissioner;
                    }
                    return data;
                });
        },
        getAll:function(){
            myAjaxs.post("../../../customer/list",
                {

                },function(data){
                    layer.msg("customer.getAll");
                    return data;
                });
        },
        getPage:function(current){
            layer.load(2);
            myAjaxs.post("../../../customer/listOne",
                {
                    current:current,
                    number:15
                },function(data){
                    layer.closeAll('loading');
                    myAjaxs.view(data);
                    return data;
                });
        },
        upOne:function(temp){
            myAjaxs.post("../../../customer/upOne",
                temp,function(data){
                    if(data==1){
                        layer.msg("更新成功")
                    }else{
                        layer.msg("更新失败")
                    }
                    return data;
                });
        }
    }