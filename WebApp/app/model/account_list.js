/**
 * Created by penny on 2016/12/24.
 */
var account_list=account_list||{
        acc_id:'',
        cus_id:'',
        acc_bankNum:'',
        acc_openBank:'',
        acc_subBank:'',
        acc_readyAccount:'',
        acc_costAccount:'',
        acc_monthAccount:'',
        getReady:function(bankNum){
            myAjaxs.post("../../../account/getReady",
                {
                    acc_bankNum:bankNum
                },function(data){
                    if(data.length==0){
                        layer.msg("密码或者用户名不存在");
                    }
                    else{
                        $('#balance').html(data. acc_readyAccount);
                    }
                    return data;
                });
        },
        login:function(acc_id){
            myAjaxs.post("../../../account/getOne",
                {
                    acc_id:acc_id
                },function(data){
                    if(data.length==0){
                        layer.msg("密码或者用户名不存在");
                    }
                    else{
                        a2=data.acc_bankNum;
                    }
                    return data;
                });
        },
        addOne:function(temp){
            myAjaxs.post("../../../account/insOne",
               temp,function(data){
                    if(data==1){
                        layer.msg("淇濆瓨鎴愬姛")
                    }else{
                        layer.msg("淇濆瓨澶辫触")
                    }
                    return data;
                });
        },
        removeOne:function(acc_id){
            myAjaxs.post("../../../account/delOne",
                {
                    acc_id:acc_id
                },function(data){
                   if(data==1){
                       layer.msg("鍒犻櫎鎴愬姛")
                   }else{
                       layer.msg("鍒犻櫎澶辫触")
                   }
                    return data;
                });
        },
        getOne:function(model,acc_id){
            myAjaxs.post("../../../account/getOne",
                {
                    acc_id:acc_id
                },function(data){
                    //layer.msg("account.getOne");
                    if(data.length==0){
                        layer.msg("涓嶅瓨鍦ㄨ璐︽埛,璇烽噸鏂拌緭鍏�");
                    }
                    else{
                        model[1].value=data.cus_id;
                        model[2].value=data.acc_bankNum;
                        model[3].value=data.acc_openBank;
                        model[4].value=data.acc_subBank;
                        model[5].value=data.acc_readyAccount;
                        model[6].value=data.acc_costAccount;
                        model[7].value=data.acc_monthAccount;
                    }
                    return data;
                });
        },
        getAll:function(){
            myAjaxs.post("../../../account/list",
                {

                },function(data){
                    for(var i=0;i<data.length; i++){
                     $('#state_list').append("<option value="+data[i].acc_bankNum+">"+data[i].acc_bankNum+"</option>");
                    }
                    return data;
                });
        },
        getPage:function(current){
            layer.load(2);
            myAjaxs.post("../../../account/listOne",
                {
                    current:current,
                    number:15
                },function(data){
                    layer.closeAll('loading');
                    myAjaxs.view(data);
                    return data;
                });
        },
        upOne:function(temp){
            myAjaxs.post("../../../account/upOne",
                temp,function(data){
                    if(data==1){
                        layer.msg("更新成功")
                    }else{
                        layer.msg("更新失败")
                    }
                    return data;
                });
        }
    }