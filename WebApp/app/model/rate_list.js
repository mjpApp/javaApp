/**
 * Created by penny on 2016/12/24.
 */
var rate_list=rate_list||{
        rate_name:'',
        rate_standard:'',
        addOne:function(){
            myAjaxs.post("../../../rate/insOne",
                {
                    rate_name:rate_list.rate_name,
                    rate_standard:rate_list.rate_standard
                },function(data){
                    layer.msg("rate.addOne");
                    return data;
                });
        },
        removeOne:function(){
            myAjaxs.post("../../../rate/delOne",
                {
                    rate_name:rate_list.rate_name
                },function(data){
                    layer.msg("rate.removeOne");
                    return data;
                });
        },
        getOne:function(rate_name){
            myAjaxs.post("../../../rate/getOne",
                {
                    rate_name:rate_name
                },function(data){
                	setRate(date);
                    layer.msg("rate.getOne");
                    return data;
                });
        },
        getAll:function(){
            myAjaxs.post("../../../rate/list",
                {

                },function(data){
                    layer.msg("rate.getAll");
                    return data;
                });
        },
        getPage:function(){
            myAjaxs.post("../../../rate/listOne",
                {
                    current:1,
                    number:10
                },function(data){
                    layer.msg("rate.getPage");
                    return data;
                });
        },
        upOne:function(){
            myAjaxs.post("../../../rate/upOne",
                {
                    rate_name:rate_list.rate_name,
                    rate_standard:rate_list.rate_standard
                },function(data){
                    layer.msg("rate.upOne");
                    return data;
                });
        }
    }