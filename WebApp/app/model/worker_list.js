/**
 * Created by penny on 2016/12/24.
 */
var worker_list = worker_list ||{
        worker_id:'',
        worker_account:'',
        worker_password:'',
        worker_name:'',
        worker_sex:'',
        worker_age:'',
        post_id:'',
        state:'',
        addOne:function () {
            myAjaxs.post("../../../worker/insOne",
                {
                    worker_id:worker_list.worker_id,
                    worker_account:worker_list.worker_account,
                    worker_password:worker_list.worker_password,
                    worker_name:worker_list.worker_name,
                    worker_sex:worker_list.worker_sex,
                    worker_age:worker_list.worker_age,
                    post_id:worker_list.post_id,
                    state:worker_list.state
                },function (data) {
                    layer.msg("worker.addOne");
                    return data;
                });
        },
        removeOne:function () {
            myAjaxs.post("../../../worker/delOne",
                {
                    worker_id:worker_list.worker_id
                },function (data) {
                    layer.msg("worker.removeOne");
                    return data;
            });
        },
        getOne:function (model,worker_id) {
            myAjaxs.post("../../../worker/getOne",
                {
                    worker_id:worker_id
                },function(data){
                    //layer.msg("customer.getOne");
                    if(data.length==0){
                        layer.msg("不存在该工作人员");
                    }
                    else{
                        model[1].value=data.worker_account;
                        model[2].value=data.worker_password;
                        model[3].value=data.worker_name;
                        model[4].value=data.worker_age;
                        model[5].value=data.worker_sex;
                        model[6].value=data.post_id;
                    }
                    return data;
                });
        },
        login:function (data) {
            myAjaxs.post("../../../worker/login",
                data,function(data){
                    //layer.msg("customer.getOne");
                    if(data.length==0){
                        layer.msg("登录失败，账号或者密码错误");
                    }
                    else{
                        layer.msg("登录成功");
                            sessionStorage.setItem('workerid',data.worker_id);
                            window.location.href='../worker/worker.html';
                    }
                    return data;
                });
        },
        getAll:function () {
            myAjaxs.post("../../../worker/list",
                {
                    
                },function (data) {
                    layer.msg("worker.getAll");
                    return data;
                });
        },
        getPage:function () {
            myAjaxs.post("../../../worker/listOne",
                {
                    current:1,
                    number:10
                },function(data){
                    layer.msg("worker.getPage");
                    return data;
                });
        },
        upOne:function(temp){
            myAjaxs.post("../../../worker/upOne",
               temp,function(data){
                    layer.msg("worker.upOne");
                    return data;
                });
        }
    }