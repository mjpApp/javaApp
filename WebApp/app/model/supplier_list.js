/**
 * Created by penny on 2016/12/24.
 */
var supplier_list=supplier_list||{
        sup_id:'',
        rate_name:'',
        sup_phone:'',
        addOne:function(){
            myAjaxs.post("../../../supplier/insOne",
                {
                    sup_id:supplier_list.sup_id,
                    rate_name:supplier_list.rate_name,
                    sup_phone:supplier_list.sup_phone
                },function(data){
                    layer.msg("supplier.addOne");
                    return data;
                });
        },
        removeOne:function(){
            myAjaxs.post("../../../supplier/delOne",
                {
                    sup_id:supplier_list.sup_id
                },function(data){
                    layer.msg("supplier.removeOne");
                    return data;
                });
        },
        getOne:function(sup_phone){
        	myAjaxs.post("../../../supplier/getOne",
        			{
        		        sup_phone:sup_phone
        			},function(data){
        				
        				layer.msg("supplier.getOne");
        				return data;
        			});
        },
        getAll:function(){
            myAjaxs.post("../../../supplier/list",
                {

                },function(data){
                    layer.msg("supplier.getAll");
                    return data;
                });
        },
        getPage:function(){
            myAjaxs.post("../../../supplier/listOne",
                {
                    current:1,
                    number:10
                },function(data){
                    layer.msg("supplier.getPage");
                    return data;
                });
        },
        upOne:function(){
            myAjaxs.post("../../../supplier/upOne",
                {
                    sup_id:supplier_list.sup_id,
                    rate_name:supplier_list.rate_name,
                    sup_phone:supplier_list.sup_phone
                },function(data){
                    layer.msg("supplier.upOne");
                    return data;
                });
        }
    }