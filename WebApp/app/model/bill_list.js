/**
 * Created by penny on 2016/12/24.
 */
var bill_list=bill_list||{
        bill_id:'',
        user_phone:'',
        bill_beCalled:'',
        bill_time:'',
        bill_money:'',
        bankNum:'',
        upbill:function(data){
            myAjaxs.post("../../../upbill",
                {
                    bill_id:data.bill_id,
                    bill_time:data.bill_time,
                    bill_money:data.bill_money,
                    bankNum:data.bankNum
                },function(data){
                    console.log(data);
                    setTime(data.bill_time);
                    setMoney(data.bill_money);
                    layer.msg("bill.send");
                    return data;
                });
        },
        listUser:function(data){
            myAjaxs.post("../../../bill/listUser",
                {
                    user_phone:data
                },function(data){
                    console.log(data);
                    show(data);
                    layer.msg("bill.send");
                    return data;
                });
        },
        addOne:function (data) {
        	layer.msg("bill.addOne");
            myAjaxs.post("../../../bill/insOne",
                {
                    bill_id:data.bill_id,
                    user_phone:data.user_phone,
                    bill_beCalled:data.bill_beCalled,
                    bill_time:data.bill_time,
                    bill_money:data.bill_money
                },function(data){
                    layer.msg("bill.addOne");
                    setId(data);
                    return data;
                });
        },
        removeOne:function () {
            myAjaxs.post("../../../bill/delOne",
                {
                    bill_id:bill_list.bill_id
                },function(data){
                    layer.msg("bill.removeOne");
                    return data;
                });
        },
        getOne:function (bill_id) {
                myAjaxs.post("../../../user/getOne",
                    {
                        bill_id:bill_id
                    },function(data){
                        if(data.length==0){
                            layer.msg("�����ڸú���,����������");
                        }
                        else{
                            model[1].value=data.user_address;
                            model[2].value=data.acc_bankNum;
                        }
                        return data;
                    });
        },
        getAll:function(){
            myAjaxs.post("../../../bill/list",
                {

                },function(data){
                    layer.msg("bill.getAll");
                    return data;
                });
        },
        getPage:function(current){
            layer.load(2);
            myAjaxs.post("../../../bill/listOne",
                {
                    current:current,
                    number:15
                },function(data){
                    layer.closeAll('loading');
                    myAjaxs.view(data);
                    return data;
                });
        },
        upOne:function(data){
            myAjaxs.post("../../../bill/upOne",
                {
                    bill_id:data.bill_id,
                    user_phone:data.user_phone,
                    bill_beCalled:data.bill_beCalled,
                    bill_time:data.bill_time,
                    bill_money:data.bill_money
                },function(data){
                    layer.msg("bill.upOne");
                    return data;
                });
        }
    }