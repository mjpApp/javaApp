package com.seehope.dao;

import java.util.List;

import com.seehope.pojo.Rate_list;

public interface IRate_listDao {
	List<Rate_list> listRate_list();

	int insOne(Rate_list user_list);

	int insAll(List<Rate_list> user_list);

	int delOne(Rate_list user_list);

	int delALL(List<Rate_list> user_list);

	Rate_list getOne(Rate_list user_list);

	List<Rate_list> list();

	List<Rate_list> listOne();

	int upOne(Rate_list user_list);

	int upAll(List<Rate_list> user_list);
}
