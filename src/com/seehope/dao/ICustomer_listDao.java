package com.seehope.dao;

import java.util.List;

import com.seehope.pojo.Customer_list;

public interface ICustomer_listDao {
	List<Customer_list> listCustomer_list();

	int insOne(Customer_list customer_list);

	int insAll(List<Customer_list> customer_list);

	int delOne(Customer_list customer_list);

	int delALL(List<Customer_list> customer_list);

	Customer_list getOne(Customer_list customer_list);

	List<Customer_list> list();

	List<Customer_list> listOne(int id);

	int upOne(Customer_list customer_list);

	int upAll(List<Customer_list> customer_list);
}
