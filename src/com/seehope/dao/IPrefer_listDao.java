package com.seehope.dao;

import java.util.List;

import com.seehope.pojo.Prefer_list;

public interface IPrefer_listDao {
	List<Prefer_list> listPrefer_list();

	int insOne(Prefer_list prefer_list);

	int insAll(List<Prefer_list> prefer_list);

	int delALL(List<Prefer_list> prefer_list);

	int delOne(Prefer_list prefer_list);

	Prefer_list getOne(Prefer_list prefer_list);

	List<Prefer_list> list();

	List<Prefer_list> listOne();

	int upOne(Prefer_list prefer_list);

	int upAll(List<Prefer_list> prefer_list);

}
