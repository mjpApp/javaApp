package com.seehope.dao;

import java.util.List;

import com.seehope.pojo.Post_list;

public interface IPost_listDao {
	List<Post_list> listPost_list();

	int insOne(Post_list post_list);

	int insAll(List<Post_list> post_list);

	int delOne(Post_list post_list);

	int delALL(List<Post_list> post_list);

	List<Post_list> list();

	Post_list getOne(Post_list post_list);

	List<Post_list> listOne();

	int upOne(Post_list post_list);

	int upAll(List<Post_list> post_list);
}
