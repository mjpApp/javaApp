package com.seehope.dao;

import java.util.List;

import com.seehope.pojo.Supplier_list;

public interface ISupplier_listDao {
	List<Supplier_list> listSupperlier_list();

	int insOne(Supplier_list supplier_list);

	int insAll(List<Supplier_list> supplier_list);

	int delOne(Supplier_list supplier_list);

	int delALL(List<Supplier_list> supplier_list);

	Supplier_list getOne(Supplier_list supplier_list);

	List<Supplier_list> list();

	List<Supplier_list> listOne();

	int upOne(Supplier_list supplier_list);

	int upAll(List<Supplier_list> supplier_list);
}
