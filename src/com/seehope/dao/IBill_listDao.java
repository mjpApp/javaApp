package com.seehope.dao;

import java.util.List;

import com.seehope.pojo.Bill_list;

public interface IBill_listDao {
	List<Bill_list> listBill_list();

	int insOne(Bill_list bill_list);

	int insAll(List<Bill_list> bill_list);

	int delOne(Bill_list bill_list);

	int delALL(List<Bill_list> bill_list);

	Bill_list getOne(Bill_list bill_list);

	List<Bill_list> list();

	List<Bill_list> listOne(int id);

	int upOne(Bill_list bill_list);

	int upAll(List<Bill_list> bill_list);

	List<Bill_list> listUser(Bill_list bill_list);
}
