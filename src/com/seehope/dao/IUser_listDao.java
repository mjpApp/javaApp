package com.seehope.dao;

import java.util.List;

import com.seehope.pojo.User_list;

public interface IUser_listDao {
	
	int insOne(User_list user_list);
	int insAll(List<User_list> user_list);
	
	int delOne(User_list user_list);
	int delALL(List<User_list> user_list);
	
	User_list getOne(User_list user_list);
	List<User_list> list();
	List<User_list> listOne(int id);
	
	int upOne(User_list user_list);
	int upAll(List<User_list> user_list);
}
