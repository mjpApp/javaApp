package com.seehope.action;

import java.util.List;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletResponse;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import com.seehope.pojo.Bill_list;
import com.seehope.pojo.page;
import com.seehope.service.IBillListService;

@Controller
@RequestMapping("/bill")
public class BillListAction {

	@Resource(name="billListService")
	private IBillListService service;
	
	@RequestMapping("/insOne")
	@ResponseBody
	public int insOne(Bill_list bill_list) {
		/**
		 * ����·����/user/insOne
		 * ��Ҫ������
		 * user_list
		 * �������ͣ�json
		 * ���أ�user_list���������ֶ�
		 */
		System.out.println(bill_list);
		return service.insOne(bill_list);
	}
	@RequestMapping("/insAll")
	@ResponseBody
	public Bill_list insAll(Bill_list bill_list) {
		/**
		 * ����·����/user/getOne
		 * ��Ҫ������user_phone
		 * �������ͣ�json
		 * ���أ�user_list���������ֶ�
		 */
		return service.getOne(bill_list);
	}
	@RequestMapping("/delOne")
	@ResponseBody
	public int delOne(Bill_list bill_list) {
		/**
		 * ����·����/user/getOne
		 * ��Ҫ������user_phone
		 * �������ͣ�json
		 * ���أ�user_list���������ֶ�
		 */
		return service.delOne(bill_list);
	}
	@RequestMapping("/delAll")
	@ResponseBody
	public int delAll(List<Bill_list> bill_list) {
		/**
		 * ����·����/user/getOne
		 * ��Ҫ������user_phone
		 * �������ͣ�json
		 * ���أ�user_list���������ֶ�
		 */
		return service.delAll(bill_list);
	}
	
	@RequestMapping("/getOne")
	@ResponseBody
	public Bill_list getOne(Bill_list bill_list) {
		/**
		 * ����·����/user/getOne
		 * ��Ҫ������user_phone
		 * �������ͣ�json
		 * ���أ�user_list���������ֶ�
		 */
		return service.getOne(bill_list);
	}
	@RequestMapping("/listOne")
	@ResponseBody
	public  List<Bill_list> listOne(page page) {
		/**
		 * ����·����/user/getOne
		 * ��Ҫ������user_phone
		
		 * ���أ�user_list���������ֶ�
		 */
		int id = (page.getCurrent()-1)*15;
		System.out.println(id);
		return service.listOne(id);
	}
	@RequestMapping("/list")
	@ResponseBody
	public List<Bill_list> list(HttpServletResponse resp) {
		System.out.println("�ҽ�����");
		return service.list();
	}
	@RequestMapping("/upOne")
	@ResponseBody
	public int upOne(Bill_list bill_list) {
		/**
		 * ����·����/user/getOne
		 * ��Ҫ������user_phone
		 * �������ͣ�json
		 * ���أ�user_list���������ֶ�
		 */
		return service.upOne(bill_list);
	}
	@RequestMapping("/upAll")
	@ResponseBody
	public int upAll(List<Bill_list> bill_list) {
		/**
		 * ����·����/user/getOne
		 * ��Ҫ������user_phone
		 * �������ͣ�json
		 * ���أ�user_list���������ֶ�
		 */
		return service.upAll(bill_list);
	}
	@RequestMapping("/listUser")
	@ResponseBody
	public List<Bill_list> listUser(Bill_list bill_list) {
		/**
		 * ����·����/user/insOne
		 * ��Ҫ������
		 * user_list
		 * �������ͣ�json
		 * ���أ�user_list���������ֶ�
		 */
		System.out.println(bill_list);
		return service.listUser(bill_list);
	}
}