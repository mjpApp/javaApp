package com.seehope.action;

import java.util.List;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletResponse;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import com.seehope.pojo.Post_list;
import com.seehope.service.IPostListService;

@Controller
@RequestMapping("/post")
public class PostListAction {

	@Resource(name="postListService")
	private IPostListService service;
	
	@RequestMapping("/insOne")
	@ResponseBody
	public int insOne(Post_list post_list) {
		/**
		 * 访问路径：/user/insOne
		 * 需要参数：
		 * user_list
		 * 返回类型：json
		 * 返回：user_list对象所有字段
		 */
		System.out.println(post_list);
		return service.insOne(post_list);
	}
	@RequestMapping("/insAll")
	@ResponseBody
	public Post_list insAll(Post_list post_list) {
		/**
		 * 访问路径：/user/getOne
		 * 需要参数：user_phone
		 * 返回类型：json
		 * 返回：user_list对象所有字段
		 */
		return service.getOne(post_list);
	}
	@RequestMapping("/delOne")
	@ResponseBody
	public int delOne(Post_list post_list) {
		/**
		 * 访问路径：/user/getOne
		 * 需要参数：user_phone
		 * 返回类型：json
		 * 返回：user_list对象所有字段
		 */
		return service.delOne(post_list);
	}
	@RequestMapping("/delAll")
	@ResponseBody
	public int delAll(List<Post_list> post_list) {
		/**
		 * 访问路径：/user/getOne
		 * 需要参数：user_phone
		 * 返回类型：json
		 * 返回：user_list对象所有字段
		 */
		return service.delAll(post_list);
	}
	
	@RequestMapping("/getOne")
	@ResponseBody
	public Post_list getOne(Post_list post_list) {
		/**
		 * 访问路径：/user/getOne
		 * 需要参数：user_phone
		 * 返回类型：json
		 * 返回：user_list对象所有字段
		 */
		return service.getOne(post_list);
	}
	@RequestMapping("/listOne")
	@ResponseBody
	public  List<Post_list> listOne(Post_list post_list) {
		/**
		 * 访问路径：/user/getOne
		 * 需要参数：user_phone
		 * 返回类型：json
		 * 返回：user_list对象所有字段
		 */
		return service.listOne();
	}
	@RequestMapping("/list")
	@ResponseBody
	public List<Post_list> list(HttpServletResponse resp) {
		System.out.println("我进来了");
		return service.list();
	}
	@RequestMapping("/upOne")
	@ResponseBody
	public int upOne(Post_list post_list) {
		/**
		 * 访问路径：/user/getOne
		 * 需要参数：user_phone
		 * 返回类型：json
		 * 返回：user_list对象所有字段
		 */
		return service.upOne(post_list);
	}
	@RequestMapping("/upAll")
	@ResponseBody
	public int upAll(List<Post_list> post_list) {
		/**
		 * 访问路径：/user/getOne
		 * 需要参数：user_phone
		 * 返回类型：json
		 * 返回：user_list对象所有字段
		 */
		return service.upAll(post_list);
	}
}
