package com.seehope.action;


import java.util.List;

import javax.annotation.Resource;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import com.seehope.pojo.Account_list;
import com.seehope.pojo.Bill_list;
import com.seehope.pojo.Rate_list;
import com.seehope.pojo.Send_list;
import com.seehope.pojo.Supplier_list;
import com.seehope.service.IAccountListService;
import com.seehope.service.IBillListService;
import com.seehope.service.IRateListService;
import com.seehope.service.ISupplierListService;

@Controller

public class SendListAction {
	@Resource(name="billListService")
	private IBillListService serviceBill;
	
	@Resource(name="supplierListService")
	private ISupplierListService serviceSupplier;
	
	@Resource(name="rateListService")
	private IRateListService serviceRate;
	
	@Resource(name="accountListService")
	private IAccountListService serviceAccount;
	
	@RequestMapping("/send")
	@ResponseBody
	public Bill_list send(Send_list send_list) {

		System.out.println(send_list);
		Supplier_list supplier_list1 = new Supplier_list();
		Supplier_list supplier_list2 = new Supplier_list();
		supplier_list1.setSup_phone(send_list.getUser_phone());
		supplier_list2.setSup_phone(send_list.getBill_beCalled());		
		System.out.println(supplier_list2);
		System.out.println(serviceSupplier.getOne(supplier_list2));
		
		
		Rate_list rate_list1 = new Rate_list();
		Rate_list rate_list2 = new Rate_list();
		rate_list1.setRate_name(serviceSupplier.getOne(supplier_list1).getRate_name());
		rate_list2.setRate_name(serviceSupplier.getOne(supplier_list2).getRate_name());
		System.out.println(rate_list1);
		double rate = Double.parseDouble(serviceRate.getOne(rate_list1).getRate_standard())+
				 Double.parseDouble(serviceRate.getOne(rate_list2).getRate_standard());
		System.out.println(rate);
		
		
		Bill_list bill_list = new Bill_list();
		bill_list.setBill_money(rate);
		bill_list.setBill_beCalled(send_list.getBill_beCalled());
		bill_list.setUser_phone(send_list.getUser_phone());
		bill_list.setBill_time(0);
		serviceBill.insOne(bill_list);
		System.out.println(bill_list);
		
		
		return bill_list;
	}
	
	@RequestMapping("/upbill")
	@ResponseBody
	public Bill_list upbill(Bill_list bill_list) {

		System.out.println(bill_list);
		double rate = bill_list.getBill_money();
		
		double money = bill_list.getBill_money() * Math.ceil((double)bill_list.getBill_time() / 60);
		bill_list.setBill_money(money);
		serviceBill.upOne(bill_list);
		
		Account_list account_list = new Account_list();
		account_list.setAcc_bankNum(bill_list.getBankNum());
		account_list= serviceAccount.getNum(account_list);
		System.out.println(account_list);
		account_list.setAcc_costAccount(account_list.getAcc_costAccount() + rate);
		account_list.setAcc_monthAccount(account_list.getAcc_monthAccount() + rate);
		account_list.setAcc_readyAccount(account_list.getAcc_readyAccount() - rate);
		serviceAccount.upCost(account_list);
		
		return serviceBill.getOne(bill_list);
	}
}
