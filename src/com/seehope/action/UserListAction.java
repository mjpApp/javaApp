package com.seehope.action;

import java.util.List;



import javax.annotation.Resource;
import javax.servlet.http.HttpServletResponse;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import com.seehope.pojo.Customer_list;
import com.seehope.pojo.User_list;
import com.seehope.pojo.page;
import com.seehope.service.IUserListService;

@Controller
@RequestMapping("/user")
public class UserListAction {

	@Resource(name="userListService")
	private IUserListService service;
	
	@RequestMapping("/insOne")
	@ResponseBody
	public int insOne(User_list user_list) {
		/**
		 * ����·����/user/insOne
		 * ��Ҫ������
		 * user_list
		 * �������ͣ�json
		 * ���أ�user_list���������ֶ�
		 */
		System.out.println(user_list);
		return service.insOne(user_list);
	}
	@RequestMapping("/insAll")
	@ResponseBody
	public User_list insAll(User_list user_list) {
		/**
		 * ����·����/user/getOne
		 * ��Ҫ������user_phone
		 * �������ͣ�json
		 * ���أ�user_list���������ֶ�
		 */
		return service.getOne(user_list);
	}
	@RequestMapping("/delOne")
	@ResponseBody
	public int delOne(User_list user_list) {
		/**
		 * ����·����/user/getOne
		 * ��Ҫ������user_phone
		 * �������ͣ�json
		 * ���أ�user_list���������ֶ�
		 */
		System.out.println(user_list);
		return service.delOne(user_list);
	}
	@RequestMapping("/delAll")
	@ResponseBody
	public int delAll(List<User_list> user_list) {
		/**
		 * ����·����/user/getOne
		 * ��Ҫ������user_phone
		 * �������ͣ�json
		 * ���أ�user_list���������ֶ�
		 */
		return service.delAll(user_list);
	}
	
	@RequestMapping("/getOne")
	@ResponseBody
	public User_list getOne(User_list user_list) {
		/**
		 * ����·����/user/getOne
		 * ��Ҫ������user_phone
		 * �������ͣ�json
		 * ���أ�user_list���������ֶ�
		 */
		return service.getOne(user_list);
	}
	@RequestMapping("/listOne")
	@ResponseBody
	public  List<User_list> listOne(page page) {
		
		int id = (page.getCurrent()-1)*15;
		System.out.println(id);
		return service.listOne(id);
	}
	@RequestMapping("/list")
	@ResponseBody
	public List<User_list> list(HttpServletResponse resp) {
		System.out.println("�ҽ�����");
		return service.list();
	}
	@RequestMapping("/upOne")
	@ResponseBody
	public int upOne(User_list user_list) {
		/**
		 * ����·����/user/getOne
		 * ��Ҫ������user_phone
		 * �������ͣ�json
		 * ���أ�user_list���������ֶ�
		 */
		return service.upOne(user_list);
	}
	
	@RequestMapping("/upAll")
	@ResponseBody
	public int upAll(List<User_list> user_list) {
		/**
		 * ����·����/user/getOne
		 * ��Ҫ������user_phone
		 * �������ͣ�json
		 * ���أ�user_list���������ֶ�
		 */
		return service.upAll(user_list);
	}
}
