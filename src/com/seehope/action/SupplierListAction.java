package com.seehope.action;

import java.util.List;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletResponse;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import com.seehope.pojo.Supplier_list;
import com.seehope.service.ISupplierListService;

@Controller
@RequestMapping("/supplier")
public class SupplierListAction {

	@Resource(name="supplierListService")
	private ISupplierListService service;
	
	@RequestMapping("/insOne")
	@ResponseBody
	public int insOne(Supplier_list supplier_list) {
		/**
		 * ����·����/user/insOne
		 * ��Ҫ������
		 * user_list
		 * �������ͣ�json
		 * ���أ�user_list���������ֶ�
		 */
		System.out.println(supplier_list);
		return service.insOne(supplier_list);
	}
	@RequestMapping("/insAll")
	@ResponseBody
	public Supplier_list insAll(Supplier_list supplier_list) {
		/**
		 * ����·����/user/getOne
		 * ��Ҫ������user_phone
		 * �������ͣ�json
		 * ���أ�user_list���������ֶ�
		 */
		return service.getOne(supplier_list);
	}
	@RequestMapping("/delOne")
	@ResponseBody
	public int delOne(Supplier_list supplier_list) {
		/**
		 * ����·����/user/getOne
		 * ��Ҫ������user_phone
		 * �������ͣ�json
		 * ���أ�user_list���������ֶ�
		 */
		return service.delOne(supplier_list);
	}
	@RequestMapping("/delAll")
	@ResponseBody
	public int delAll(List<Supplier_list> supplier_list) {
		/**
		 * ����·����/user/getOne
		 * ��Ҫ������user_phone
		 * �������ͣ�json
		 * ���أ�user_list���������ֶ�
		 */
		return service.delAll(supplier_list);
	}
	
	@RequestMapping("/getOne")
	@ResponseBody
	public Supplier_list getOne(Supplier_list supplier_list) {
		/**
		 * ����·����/user/getOne
		 * ��Ҫ������user_phone
		 * �������ͣ�json
		 * ���أ�user_list���������ֶ�
		 */
		System.out.println(supplier_list);
		return service.getOne(supplier_list);
	}
	@RequestMapping("/listOne")
	@ResponseBody
	public  List<Supplier_list> listOne(Supplier_list supplier_list) {
		/**
		 * ����·����/user/getOne
		 * ��Ҫ������user_phone
		 * �������ͣ�json
		 * ���أ�user_list���������ֶ�
		 */
		return service.listOne();
	}
	@RequestMapping("/list")
	@ResponseBody
	public List<Supplier_list> list(HttpServletResponse resp) {
		System.out.println("�ҽ�����");
		return service.list();
	}
	@RequestMapping("/upOne")
	@ResponseBody
	public int upOne(Supplier_list supplier_list) {
		/**
		 * ����·����/user/getOne
		 * ��Ҫ������user_phone
		 * �������ͣ�json
		 * ���أ�user_list���������ֶ�
		 */
		return service.upOne(supplier_list);
	}
	@RequestMapping("/upAll")
	@ResponseBody
	public int upAll(List<Supplier_list> supplier_list) {
		/**
		 * ����·����/user/getOne
		 * ��Ҫ������user_phone
		 * �������ͣ�json
		 * ���أ�user_list���������ֶ�
		 */
		return service.upAll(supplier_list);
	}
}
