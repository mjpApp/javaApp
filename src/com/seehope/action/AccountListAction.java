package com.seehope.action;

import java.util.List;


import javax.annotation.Resource;
import javax.servlet.http.HttpServletResponse;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import com.seehope.pojo.Account_list;
import com.seehope.pojo.page;
import com.seehope.service.IAccountListService;

@Controller
@RequestMapping("/account")
public class AccountListAction {

	@Resource(name="accountListService")
	private IAccountListService service;
	
	@RequestMapping("/insOne")
	@ResponseBody
	public int insOne(Account_list account_list) {
		/**
		 * ����·����/user/insOne
		 * ��Ҫ������
		 * user_list
		 * �������ͣ�json
		 * ���أ�user_list���������ֶ�
		 */
		System.out.println(account_list);
		return service.insOne(account_list);
	}
	@RequestMapping("/insAll")
	@ResponseBody
	public Account_list insAll(Account_list account_list) {
		/**
		 * ����·����/user/getOne
		 * ��Ҫ������user_phone
		 * �������ͣ�json
		 * ���أ�user_list���������ֶ�
		 */
		return service.getOne(account_list);
	}
	@RequestMapping("/delOne")
	@ResponseBody
	public int delOne(Account_list account_list) {
		/**
		 * ����·����/user/getOne
		 * ��Ҫ������user_phone
		 * �������ͣ�json
		 * ���أ�user_list���������ֶ�
		 */
	
		return service.delOne(account_list);
	}
	@RequestMapping("/delAll")
	@ResponseBody
	public int delAll(List<Account_list> account_list) {
		/**
		 * ����·����/user/getOne
		 * ��Ҫ������user_phone
		 * �������ͣ�json
		 * ���أ�user_list���������ֶ�
		 */
		return service.delAll(account_list);
	}
	
	@RequestMapping("/getOne")
	@ResponseBody
	public Account_list getOne(Account_list account_list) {
		/**
		 * ����·����/user/getOne
		 * ��Ҫ������user_phone
		 * �������ͣ�json
		 * ���أ�user_list���������ֶ�
		 */
		System.out.println(account_list);
		return service.getOne(account_list);
		
	}
	@RequestMapping("/listOne")
	@ResponseBody
	public  List<Account_list> listOne(page page) {
		/**
		 * ����·����/user/getOne
		 * ��Ҫ������user_phone
		 * �������ͣ�json
		 * ���أ�user_list���������ֶ�
		 */
		int id = (page.getCurrent()-1)*15;
		System.out.println(id);
		return service.listOne(id);
	}
	@RequestMapping("/list")
	@ResponseBody
	public List<Account_list> list(HttpServletResponse resp) {
		System.out.println("�ҽ�����");
		return service.list();
	}
	@RequestMapping("/upOne")
	@ResponseBody
	public int upOne(Account_list account_list) {
		/**
		 * ����·����/user/getOne
		 * ��Ҫ������user_phone
		 * �������ͣ�json
		 * ���أ�user_list���������ֶ�
		 */
		System.out.println(account_list);
		return service.upOne(account_list);
	}
	@RequestMapping("/upAll")
	@ResponseBody
	public int upAll(List<Account_list> account_list) {
		/**
		 * ����·����/user/getOne
		 * ��Ҫ������user_phone
		 * �������ͣ�json
		 * ���أ�user_list���������ֶ�
		 */
		return service.upAll(account_list);
	}
	@RequestMapping("/getReady")
	@ResponseBody
	public Account_list getReady(Account_list account_list) {
		/**
		 * ����·����/user/getOne
		 * ��Ҫ������user_phone
		 * �������ͣ�json
		 * ���أ�user_list���������ֶ�
		 */
		System.out.println(account_list);
		return service.getNum(account_list);
	}
}
