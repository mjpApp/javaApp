package com.seehope.action;

import java.util.List;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletResponse;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import com.seehope.pojo.Customer_list;
import com.seehope.pojo.page;
import com.seehope.service.ICustomerListService;

@Controller
@RequestMapping("/customer")
public class CustomerListAction {

	@Resource(name="customerListService")
	private ICustomerListService service;
	
	@RequestMapping("/insOne")
	@ResponseBody
	public int insOne(Customer_list customer_list) {
		/**
		 * ����·����/user/insOne
		 * ��Ҫ������
		 * user_list
		 * �������ͣ�json
		 * ���أ�user_list���������ֶ�
		 */
		System.out.println(customer_list);
		return service.insOne(customer_list);
	}
	@RequestMapping("/insAll")
	@ResponseBody
	public Customer_list insAll(Customer_list customer_list) {
		/**
		 * ����·����/user/getOne
		 * ��Ҫ������user_phone
		 * �������ͣ�json
		 * ���أ�user_list���������ֶ�
		 */
		return service.getOne(customer_list);
	}
	@RequestMapping("/delOne")
	@ResponseBody
	public int delOne(Customer_list customer_list) {
		/**
		 * ����·����/user/getOne
		 * ��Ҫ������user_phone
		 * �������ͣ�json
		 * ���أ�user_list���������ֶ�
		 */
		return service.delOne(customer_list);
	}
	@RequestMapping("/delAll")
	@ResponseBody
	public int delAll(List<Customer_list> customer_list) {
		/**
		 * ����·����/user/getOne
		 * ��Ҫ������user_phone
		 * �������ͣ�json
		 * ���أ�user_list���������ֶ�
		 */
		return service.delAll(customer_list);
	}
	
	@RequestMapping("/getOne")
	@ResponseBody
	public Customer_list getOne(Customer_list customer_list) {
		/**
		 * ����·����/user/getOne
		 * ��Ҫ������user_phone
		 * �������ͣ�json
		 * ���أ�user_list���������ֶ�
		 */
		System.out.println(customer_list);
		return service.getOne(customer_list);
	}
	@RequestMapping("/listOne")
	@ResponseBody
	public  List<Customer_list> listOne(page page) {
		/**
		 * ����·����/user/getOne
		 * ��Ҫ������user_phone
		 * �������ͣ�json
		 * ���أ�user_list���������ֶ�
		 */
		int id = (page.getCurrent()-1)*15;
		System.out.println(id);
		return service.listOne(id);
	}
	@RequestMapping("/list")
	@ResponseBody
	public List<Customer_list> list(HttpServletResponse resp) {
		System.out.println("�ҽ�����");
		return service.list();
	}
	@RequestMapping("/upOne")
	@ResponseBody
	public int upOne(Customer_list customer_list) {
		/**
		 * ����·����/user/getOne
		 * ��Ҫ������user_phone
		 * �������ͣ�json
		 * ���أ�user_list���������ֶ�
		 */
		return service.upOne(customer_list);
	}
	@RequestMapping("/upAll")
	@ResponseBody
	public int upAll(List<Customer_list> customer_list) {
		/**
		 * ����·����/user/getOne
		 * ��Ҫ������user_phone
		 * �������ͣ�json
		 * ���أ�user_list���������ֶ�
		 */
		return service.upAll(customer_list);
	}
}
