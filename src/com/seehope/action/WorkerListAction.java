package com.seehope.action;

import java.util.List;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletResponse;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import com.seehope.pojo.Worker_list;
import com.seehope.service.IWorkerListService;

@Controller
@RequestMapping("/worker")
public class WorkerListAction {

	@Resource(name="workerListService")
	private IWorkerListService service;
	
	@RequestMapping("/insOne")
	@ResponseBody
	public int insOne(Worker_list worker_list) {
		/**
		 * ����·����/user/insOne
		 * ��Ҫ������
		 * user_list
		 * �������ͣ�json
		 * ���أ�user_list���������ֶ�
		 */
		System.out.println(worker_list);
		return service.insOne(worker_list);
	}
	@RequestMapping("/insAll")
	@ResponseBody
	public Worker_list insAll(Worker_list worker_list) {
		/**
		 * ����·����/user/getOne
		 * ��Ҫ������user_phone
		 * �������ͣ�json
		 * ���أ�user_list���������ֶ�
		 */
		return service.getOne(worker_list);
	}
	@RequestMapping("/delOne")
	@ResponseBody
	public int delOne(Worker_list worker_list) {
		/**
		 * ����·����/user/getOne
		 * ��Ҫ������user_phone
		 * �������ͣ�json
		 * ���أ�user_list���������ֶ�
		 */
		return service.delOne(worker_list);
	}
	@RequestMapping("/delAll")
	@ResponseBody
	public int delAll(List<Worker_list> worker_list) {
		/**
		 * ����·����/user/getOne
		 * ��Ҫ������user_phone
		 * �������ͣ�json
		 * ���أ�user_list���������ֶ�
		 */
		return service.delAll(worker_list);
	}
	
	@RequestMapping("/getOne")
	@ResponseBody
	public Worker_list getOne(Worker_list worker_list) {
		/**
		 * ����·����/user/getOne
		 * ��Ҫ������user_phone
		 * �������ͣ�json
		 * ���أ�user_list���������ֶ�
		 */
		return service.getOne(worker_list);
	}
	@RequestMapping("/listOne")
	@ResponseBody
	public  List<Worker_list> listOne(Worker_list worker_list) {
		/**
		 * ����·����/user/getOne
		 * ��Ҫ������user_phone
		 * �������ͣ�json
		 * ���أ�user_list���������ֶ�
		 */
		return service.listOne();
	}
	@RequestMapping("/list")
	@ResponseBody
	public List<Worker_list> list(HttpServletResponse resp) {
		System.out.println("�ҽ�����");
		return service.list();
	}
	@RequestMapping("/upOne")
	@ResponseBody
	public int upOne(Worker_list worker_list) {
		/**
		 * ����·����/user/getOne
		 * ��Ҫ������user_phone
		 * �������ͣ�json
		 * ���أ�user_list���������ֶ�
		 */
		return service.upOne(worker_list);
	}
	@RequestMapping("/upAll")
	@ResponseBody
	public int upAll(List<Worker_list> worker_list) {
		/**
		 * ����·����/user/getOne
		 * ��Ҫ������user_phone
		 * �������ͣ�json
		 * ���أ�user_list���������ֶ�
		 */
		return service.upAll(worker_list);
	}
	@RequestMapping("/login")
	@ResponseBody
	public Worker_list login(Worker_list worker_list) {
		/**
		 * ����·����/user/getOne
		 * ��Ҫ������user_phone
		 * �������ͣ�json
		 * ���أ�user_list���������ֶ�
		 */
		return service.login(worker_list);
	}
}