package com.seehope.action;

import java.util.List;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletResponse;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import com.seehope.pojo.Diary_list;
import com.seehope.service.IDiaryListService;

@Controller
@RequestMapping("/diary")
public class DiaryListAction {

	@Resource(name="diaryListService")
	private IDiaryListService service;
	
	@RequestMapping("/insOne")
	@ResponseBody
	public int insOne(Diary_list diary_list) {
		/**
		 * 访问路径：/user/insOne
		 * 需要参数：
		 * user_list
		 * 返回类型：json
		 * 返回：user_list对象所有字段
		 */
		System.out.println(diary_list);
		return service.insOne(diary_list);
	}
	@RequestMapping("/insAll")
	@ResponseBody
	public Diary_list insAll(Diary_list diary_list) {
		/**
		 * 访问路径：/user/getOne
		 * 需要参数：user_phone
		 * 返回类型：json
		 * 返回：user_list对象所有字段
		 */
		return service.getOne(diary_list);
	}
	@RequestMapping("/delOne")
	@ResponseBody
	public int delOne(Diary_list diary_list) {
		/**
		 * 访问路径：/user/getOne
		 * 需要参数：user_phone
		 * 返回类型：json
		 * 返回：user_list对象所有字段
		 */
		return service.delOne(diary_list);
	}
	@RequestMapping("/delAll")
	@ResponseBody
	public int delAll(List<Diary_list> diary_list) {
		/**
		 * 访问路径：/user/getOne
		 * 需要参数：user_phone
		 * 返回类型：json
		 * 返回：user_list对象所有字段
		 */
		return service.delAll(diary_list);
	}
	
	@RequestMapping("/getOne")
	@ResponseBody
	public Diary_list getOne(Diary_list diary_list) {
		/**
		 * 访问路径：/user/getOne
		 * 需要参数：user_phone
		 * 返回类型：json
		 * 返回：user_list对象所有字段
		 */
		return service.getOne(diary_list);
	}
	@RequestMapping("/listOne")
	@ResponseBody
	public  List<Diary_list> listOne(Diary_list diary_list) {
		/**
		 * 访问路径：/user/getOne
		 * 需要参数：user_phone
		 * 返回类型：json
		 * 返回：user_list对象所有字段
		 */
		return service.listOne();
	}
	@RequestMapping("/list")
	@ResponseBody
	public List<Diary_list> list(HttpServletResponse resp) {
		System.out.println("我进来了");
		return service.list();
	}
	@RequestMapping("/upOne")
	@ResponseBody
	public int upOne(Diary_list diary_list) {
		/**
		 * 访问路径：/user/getOne
		 * 需要参数：user_phone
		 * 返回类型：json
		 * 返回：user_list对象所有字段
		 */
		return service.upOne(diary_list);
	}
	@RequestMapping("/upAll")
	@ResponseBody
	public int upAll(List<Diary_list> diary_list) {
		/**
		 * 访问路径：/user/getOne
		 * 需要参数：user_phone
		 * 返回类型：json
		 * 返回：user_list对象所有字段
		 */
		return service.upAll(diary_list);
	}
}
