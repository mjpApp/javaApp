package com.seehope.action;

import java.util.List;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletResponse;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import com.seehope.pojo.Prefer_list;
import com.seehope.service.IPreferListService;

@Controller
@RequestMapping("/prefer")
public class PreferListAction {

	@Resource(name="preferListService")
	private IPreferListService service;
	
	@RequestMapping("/insOne")
	@ResponseBody
	public int insOne(Prefer_list prefer_list) {
		/**
		 * 访问路径：/user/insOne
		 * 需要参数：
		 * user_list
		 * 返回类型：json
		 * 返回：user_list对象所有字段
		 */
		System.out.println(prefer_list);
		return service.insOne(prefer_list);
	}
	@RequestMapping("/insAll")
	@ResponseBody
	public Prefer_list insAll(Prefer_list prefer_list) {
		/**
		 * 访问路径：/user/getOne
		 * 需要参数：user_phone
		 * 返回类型：json
		 * 返回：user_list对象所有字段
		 */
		return service.getOne(prefer_list);
	}
	@RequestMapping("/delOne")
	@ResponseBody
	public int delOne(Prefer_list prefer_list) {
		/**
		 * 访问路径：/user/getOne
		 * 需要参数：user_phone
		 * 返回类型：json
		 * 返回：user_list对象所有字段
		 */
		return service.delOne(prefer_list);
	}
	@RequestMapping("/delAll")
	@ResponseBody
	public int delAll(List<Prefer_list> prefer_list) {
		/**
		 * 访问路径：/user/getOne
		 * 需要参数：user_phone
		 * 返回类型：json
		 * 返回：user_list对象所有字段
		 */
		return service.delAll(prefer_list);
	}
	
	@RequestMapping("/getOne")
	@ResponseBody
	public Prefer_list getOne(Prefer_list prefer_list) {
		/**
		 * 访问路径：/user/getOne
		 * 需要参数：user_phone
		 * 返回类型：json
		 * 返回：user_list对象所有字段
		 */
		return service.getOne(prefer_list);
	}
	@RequestMapping("/listOne")
	@ResponseBody
	public  List<Prefer_list> listOne(Prefer_list prefer_list) {
		/**
		 * 访问路径：/user/getOne
		 * 需要参数：user_phone
		 * 返回类型：json
		 * 返回：user_list对象所有字段
		 */
		return service.listOne();
	}
	@RequestMapping("/list")
	@ResponseBody
	public List<Prefer_list> list(HttpServletResponse resp) {
		System.out.println("我进来了");
		return service.list();
	}
	@RequestMapping("/upOne")
	@ResponseBody
	public int upOne(Prefer_list prefer_list) {
		/**
		 * 访问路径：/user/getOne
		 * 需要参数：user_phone
		 * 返回类型：json
		 * 返回：user_list对象所有字段
		 */
		return service.upOne(prefer_list);
	}
	@RequestMapping("/upAll")
	@ResponseBody
	public int upAll(List<Prefer_list> prefer_list) {
		/**
		 * 访问路径：/user/getOne
		 * 需要参数：user_phone
		 * 返回类型：json
		 * 返回：user_list对象所有字段
		 */
		return service.upAll(prefer_list);
	}
}
