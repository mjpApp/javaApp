package com.seehope.action;

import java.util.List;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletResponse;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import com.seehope.pojo.Rate_list;
import com.seehope.service.IRateListService;

@Controller
@RequestMapping("/rate")
public class RateListAction {

	@Resource(name="rateListService")
	private IRateListService service;
	
	@RequestMapping("/insOne")
	@ResponseBody
	public int insOne(Rate_list rate_list) {
		/**
		 * 访问路径：/user/insOne
		 * 需要参数：
		 * user_list
		 * 返回类型：json
		 * 返回：user_list对象所有字段
		 */
		System.out.println(rate_list);
		return service.insOne(rate_list);
	}
	@RequestMapping("/insAll")
	@ResponseBody
	public Rate_list insAll(Rate_list rate_list) {
		/**
		 * 访问路径：/user/getOne
		 * 需要参数：user_phone
		 * 返回类型：json
		 * 返回：user_list对象所有字段
		 */
		return service.getOne(rate_list);
	}
	@RequestMapping("/delOne")
	@ResponseBody
	public int delOne(Rate_list rate_list) {
		/**
		 * 访问路径：/user/getOne
		 * 需要参数：user_phone
		 * 返回类型：json
		 * 返回：user_list对象所有字段
		 */
		return service.delOne(rate_list);
	}
	@RequestMapping("/delAll")
	@ResponseBody
	public int delAll(List<Rate_list> rate_list) {
		/**
		 * 访问路径：/user/getOne
		 * 需要参数：user_phone
		 * 返回类型：json
		 * 返回：user_list对象所有字段
		 */
		return service.delAll(rate_list);
	}
	
	@RequestMapping("/getOne")
	@ResponseBody
	public Rate_list getOne(Rate_list rate_list) {
		/**
		 * 访问路径：/user/getOne
		 * 需要参数：user_phone
		 * 返回类型：json
		 * 返回：user_list对象所有字段
		 */
		return service.getOne(rate_list);
	}
	@RequestMapping("/listOne")
	@ResponseBody
	public  List<Rate_list> listOne(Rate_list rate_list) {
		/**
		 * 访问路径：/user/getOne
		 * 需要参数：user_phone
		 * 返回类型：json
		 * 返回：user_list对象所有字段
		 */
		return service.listOne();
	}
	@RequestMapping("/list")
	@ResponseBody
	public List<Rate_list> list(HttpServletResponse resp) {
		System.out.println("我进来了");
		return service.list();
	}
	@RequestMapping("/upOne")
	@ResponseBody
	public int upOne(Rate_list rate_list) {
		/**
		 * 访问路径：/user/getOne
		 * 需要参数：user_phone
		 * 返回类型：json
		 * 返回：user_list对象所有字段
		 */
		return service.upOne(rate_list);
	}
	@RequestMapping("/upAll")
	@ResponseBody
	public int upAll(List<Rate_list> rate_list) {
		/**
		 * 访问路径：/user/getOne
		 * 需要参数：user_phone
		 * 返回类型：json
		 * 返回：user_list对象所有字段
		 */
		return service.upAll(rate_list);
	}
}
