package com.seehope.pojo;

public class Send_list {
	private String user_phone;
	private String bill_beCalled;
	private String acc_bankNum;
	public String getUser_phone() {
		return user_phone;
	}
	public void setUser_phone(String user_phone) {
		this.user_phone = user_phone;
	}
	public String getBill_beCalled() {
		return bill_beCalled;
	}
	public void setBill_beCalled(String bill_beCalled) {
		this.bill_beCalled = bill_beCalled;
	}
	public String getAcc_bankNum() {
		return acc_bankNum;
	}
	public void setAcc_bankNum(String acc_bankNum) {
		this.acc_bankNum = acc_bankNum;
	}
	public Send_list() {
		
	}

	public String toString() {
		return "Send_list [user_phone=" + user_phone + ", bill_beCalled=" + bill_beCalled + ", acc_bankNum="
				+ acc_bankNum + "]";
	}
	
	
}
