package com.seehope.pojo;

public class User_list {
	private String user_phone;
	private String user_address;//有错user_address
	private String acc_bankNum;//这里帮你改了
	public String getUser_phone() {
		return user_phone;
	}
	public void setUser_phone(String user_phone) {
		this.user_phone = user_phone;
	}
	public String getUser_address() {
		return user_address;
	}
	public void setUser_address(String user_address) {
		this.user_address = user_address;
	}
	public String getAcc_bankNum() {
		return acc_bankNum;
	}
	public void setAcc_bankNum(String acc_bankNum) {
		this.acc_bankNum = acc_bankNum;
	}
	public User_list() {
		
	}
	@Override
	public String toString() {
		return "User_list [user_phone=" + user_phone + ", user_address=" + user_address + ", acc_bankNum=" + acc_bankNum
				+ "]";
	}


}
