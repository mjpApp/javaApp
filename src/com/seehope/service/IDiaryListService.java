package com.seehope.service;

import java.util.List;

import com.seehope.pojo.Diary_list;

public interface IDiaryListService {
	int insOne(Diary_list diary_list);
	int insAll(List<Diary_list> diary_list);
	
	int delOne(Diary_list diary_list);
	int delAll(List<Diary_list> diary_list);
	
	Diary_list getOne(Diary_list diary_list);
	List<Diary_list> list();
	List<Diary_list> listOne();
	
	int upOne(Diary_list diary_list);
	int upAll(List<Diary_list> diary_list);
}
