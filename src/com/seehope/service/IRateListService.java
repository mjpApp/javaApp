package com.seehope.service;

import java.util.List;

import com.seehope.pojo.Rate_list;

public interface IRateListService {
	int insOne(Rate_list rate_list);
	int insAll(List<Rate_list> rate_list);
	
	int delOne(Rate_list rate_list);
	int delAll(List<Rate_list> rate_list);
	
	Rate_list getOne(Rate_list rate_list);
	List<Rate_list> list();
	List<Rate_list> listOne();
	
	int upOne(Rate_list rate_list);
	int upAll(List<Rate_list> rate_list);

}
