package com.seehope.service;

import java.util.List;

import com.seehope.pojo.Account_list;

public interface IAccountListService {
	int insOne(Account_list account_list);
	
	int insAll(List<Account_list> account_list);
	
	int delOne(Account_list user_list);
	int delAll(List<Account_list> account_list);
	
	Account_list getOne(Account_list account_list);
	List<Account_list> list();
	List<Account_list> listOne(int id);
	
	int upOne(Account_list account_list);
	int upAll(List<Account_list> account_list);

	int upCost(Account_list account_list);

	Account_list getNum(Account_list account_list);
}
