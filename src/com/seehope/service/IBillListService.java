package com.seehope.service;

import java.util.List;

import com.seehope.pojo.Bill_list;

public interface IBillListService {
	int insOne(Bill_list bill_list);
	int insAll(List<Bill_list> bill_list);
	
	int delOne(Bill_list bill_list);
	int delAll(List<Bill_list> bill_list);
	
	Bill_list getOne(Bill_list bill_list);
	List<Bill_list> list();
	List<Bill_list> listOne(int id);
	
	int upOne(Bill_list bill_list);
	int upAll(List<Bill_list> bill_list);
	List<Bill_list> listUser(Bill_list bill_list);

}
