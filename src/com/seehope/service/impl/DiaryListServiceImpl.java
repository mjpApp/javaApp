package com.seehope.service.impl;

import java.util.List;

import javax.annotation.Resource;

import org.springframework.stereotype.Service;

import com.seehope.dao.IDiary_listDao;
import com.seehope.pojo.Diary_list;
import com.seehope.service.IDiaryListService;

@Service("diaryListService")
public class DiaryListServiceImpl implements IDiaryListService {
	
	
	@Resource
	private IDiary_listDao dao;
	
//	��
	public int insOne(Diary_list diary_list) {
		return dao.insOne(diary_list);
	}
	public int insAll(List<Diary_list> diary_list) {
		return dao.insAll(diary_list);
		
	}
//	ɾ
	public int delOne(Diary_list diary_list) {
		return dao.delOne(diary_list);
		
	}

	public int delAll(List<Diary_list> diary_list) {
		return dao.delALL(diary_list);
		
	}
//	��
	public Diary_list getOne(Diary_list diary_list) {
		return dao.getOne(diary_list);
		
	}
	
	public List<Diary_list> list() {
		return dao.list();
	}
// ��ҳ
	public List<Diary_list> listOne() {
		
		return dao.listOne();
	}
// ��
	public int upOne(Diary_list diary_list) {
		return dao.upOne(diary_list);
	}

	public int upAll(List<Diary_list> diary_list) {
	
		return dao.upAll(diary_list);
	}
}
