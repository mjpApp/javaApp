package com.seehope.service.impl;

import java.util.List;

import javax.annotation.Resource;

import org.springframework.stereotype.Service;

import com.seehope.dao.IBill_listDao;
import com.seehope.pojo.Bill_list;
import com.seehope.service.IBillListService;

@Service("billListService")
public class BillListServiceImpl implements IBillListService {
	
	
	@Resource
	private IBill_listDao dao;
	
//	��
	public int insOne(Bill_list bill_list) {
		return dao.insOne(bill_list);
	}
	public int insAll(List<Bill_list> bill_list) {
		return dao.insAll(bill_list);
		
	}
//	ɾ
	public int delOne(Bill_list bill_list) {
		return dao.delOne(bill_list);
		
	}

	public int delAll(List<Bill_list> bill_list) {
		return dao.delALL(bill_list);
		
	}
//	��
	public Bill_list getOne(Bill_list bill_list) {
		return dao.getOne(bill_list);
		
	}
	
	public List<Bill_list> list() {
		return dao.list();
	}
// ��ҳ
	public List<Bill_list> listOne(int id) {
		
		return dao.listOne(id);
	}
// ��
	public int upOne(Bill_list bill_list) {
		return dao.upOne(bill_list);
	}

	public int upAll(List<Bill_list> bill_list) {
	
		return dao.upAll(bill_list);
	}
	@Override
	public List<Bill_list> listUser(Bill_list bill_list) {
		// TODO Auto-generated method stub
		return dao.listUser(bill_list);
	}
}
