package com.seehope.service.impl;

import java.util.List;

import javax.annotation.Resource;

import org.springframework.stereotype.Service;

import com.seehope.dao.IRate_listDao;
import com.seehope.pojo.Rate_list;
import com.seehope.service.IRateListService;

@Service("rateListService")
public class RateListServiceImpl implements IRateListService {
	
	
	@Resource
	private IRate_listDao dao;
	
//	��
	public int insOne(Rate_list rate_list) {
		return dao.insOne(rate_list);
	}
	public int insAll(List<Rate_list> rate_list) {
		return dao.insAll(rate_list);
		
	}
//	ɾ
	public int delOne(Rate_list rate_list) {
		return dao.delOne(rate_list);
		
	}

	public int delAll(List<Rate_list> rate_list) {
		return dao.delALL(rate_list);
		
	}
//	��
	public Rate_list getOne(Rate_list rate_list) {
		return dao.getOne(rate_list);
		
	}
	
	public List<Rate_list> list() {
		return dao.list();
	}
// ��ҳ
	public List<Rate_list> listOne() {
		
		return dao.listOne();
	}
// ��
	public int upOne(Rate_list rate_list) {
		return dao.upOne(rate_list);
	}

	public int upAll(List<Rate_list> rate_list) {
	
		return dao.upAll(rate_list);
	}
}

