package com.seehope.service.impl;

import java.util.List;

import javax.annotation.Resource;

import org.springframework.stereotype.Service;

import com.seehope.dao.ISupplier_listDao;
import com.seehope.pojo.Supplier_list;
import com.seehope.service.ISupplierListService;

@Service("supplierListService")
public class SupplierListServiceImpl implements ISupplierListService {
	
	
	@Resource
	private ISupplier_listDao dao;
	
//	��
	public int insOne(Supplier_list supplier_list) {
		return dao.insOne(supplier_list);
	}
	public int insAll(List<Supplier_list> supplier_list) {
		return dao.insAll(supplier_list);
		
	}
//	ɾ
	public int delOne(Supplier_list supplier_list) {
		return dao.delOne(supplier_list);
		
	}

	public int delAll(List<Supplier_list> supplier_list) {
		return dao.delALL(supplier_list);
		
	}
//	��
	public Supplier_list getOne(Supplier_list supplier_list) {
		return dao.getOne(supplier_list);
		
	}
	
	public List<Supplier_list> list() {
		return dao.list();
	}
// ��ҳ
	public List<Supplier_list> listOne() {
		
		return dao.listOne();
	}
// ��
	public int upOne(Supplier_list supplier_list) {
		return dao.upOne(supplier_list);
	}

	public int upAll(List<Supplier_list> supplier_list) {
	
		return dao.upAll(supplier_list);
	}
}

