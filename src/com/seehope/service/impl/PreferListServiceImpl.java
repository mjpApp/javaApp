package com.seehope.service.impl;

import java.util.List;

import javax.annotation.Resource;

import org.springframework.stereotype.Service;

import com.seehope.dao.IPrefer_listDao;
import com.seehope.pojo.Prefer_list;
import com.seehope.service.IPreferListService;

@Service("preferListService")
public class PreferListServiceImpl implements IPreferListService {
	
	
	@Resource
	private IPrefer_listDao dao;
	
//	��
	public int insOne(Prefer_list prefer_list) {
		return dao.insOne(prefer_list);
	}
	public int insAll(List<Prefer_list> prefer_list) {
		return dao.insAll(prefer_list);
		
	}
//	ɾ
	public int delOne(Prefer_list prefer_list) {
		return dao.delOne(prefer_list);
		
	}

	public int delAll(List<Prefer_list> prefer_list) {
		return dao.delALL(prefer_list);
		
	}
//	��
	public Prefer_list getOne(Prefer_list prefer_list) {
		return dao.getOne(prefer_list);
		
	}
	
	public List<Prefer_list> list() {
		return dao.list();
	}
// ��ҳ
	public List<Prefer_list> listOne() {
		
		return dao.listOne();
	}
// ��
	public int upOne(Prefer_list prefer_list) {
		return dao.upOne(prefer_list);
	}

	public int upAll(List<Prefer_list> prefer_list) {
	
		return dao.upAll(prefer_list);
	}
}
