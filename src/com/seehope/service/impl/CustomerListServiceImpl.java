package com.seehope.service.impl;

import java.util.List;

import javax.annotation.Resource;

import org.springframework.stereotype.Service;

import com.seehope.dao.ICustomer_listDao;
import com.seehope.pojo.Customer_list;
import com.seehope.service.ICustomerListService;

@Service("customerListService")
public class CustomerListServiceImpl implements ICustomerListService {
	
	
	@Resource
	private ICustomer_listDao dao;
	
//	��
	public int insOne(Customer_list customer_list) {
		return dao.insOne(customer_list);
	}
	public int insAll(List<Customer_list> customer_list) {
		return dao.insAll(customer_list);
		
	}
//	ɾ
	public int delOne(Customer_list customer_list) {
		return dao.delOne(customer_list);
		
	}

	public int delAll(List<Customer_list> customer_list) {
		return dao.delALL(customer_list);
		
	}
//	��
	public Customer_list getOne(Customer_list customer_list) {
		return dao.getOne(customer_list);
		
	}
	
	public List<Customer_list> list() {
		return dao.list();
	}
// ��ҳ
	public List<Customer_list> listOne(int id) {
		
		return dao.listOne(id);
	}
// ��
	public int upOne(Customer_list customer_list) {
		return dao.upOne(customer_list);
	}

	public int upAll(List<Customer_list> customer_list) {
	
		return dao.upAll(customer_list);
	}
}

