package com.seehope.service.impl;



import java.util.List;



import javax.annotation.Resource;

import org.springframework.stereotype.Service;

import com.seehope.dao.IUser_listDao;
import com.seehope.pojo.User_list;
import com.seehope.service.IUserListService;

@Service("userListService")
public class UserListServiceImpl implements IUserListService {
	
	
	@Resource
	private IUser_listDao dao;
	
//	��
	public int insOne(User_list user_list) {
		return dao.insOne(user_list);
	}
	public int insAll(List<User_list> user_list) {
		return dao.insAll(user_list);
		
	}
//	ɾ
	public int delOne(User_list user_list) {
		return dao.delOne(user_list);
		
	}

	public int delAll(List<User_list> user_list) {
		return dao.delALL(user_list);
		
	}
//	��
	public User_list getOne(User_list user_list) {
		return dao.getOne(user_list);
		
	}
	
	public List<User_list> list() {
		return dao.list();
	}
// ��ҳ
	public List<User_list> listOne(int id) {
		
		return dao.listOne(id);
	}
// ��
	public int upOne(User_list user_list) {
		return dao.upOne(user_list);
	}

	public int upAll(List<User_list> user_list) {
	
		return dao.upAll(user_list);
	}
}
