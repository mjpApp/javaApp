package com.seehope.service.impl;

import java.util.List;

import javax.annotation.Resource;

import org.springframework.stereotype.Service;

import com.seehope.dao.IWorker_listDao;
import com.seehope.pojo.Worker_list;
import com.seehope.service.IWorkerListService;

@Service("workerListService")
public class WorkerListServiceImpl implements IWorkerListService {
	
	
	@Resource
	private IWorker_listDao dao;
	
//	��
	public int insOne(Worker_list worker_list) {
		return dao.insOne(worker_list);
	}
	public int insAll(List<Worker_list> worker_list) {
		return dao.insAll(worker_list);
		
	}
//	ɾ
	public int delOne(Worker_list worker_list) {
		return dao.delOne(worker_list);
		
	}

	public int delAll(List<Worker_list> worker_list) {
		return dao.delALL(worker_list);
		
	}
//	��
	public Worker_list getOne(Worker_list worker_list) {
		return dao.getOne(worker_list);
		
	}
	
	public List<Worker_list> list() {
		return dao.list();
	}
// ��ҳ
	public List<Worker_list> listOne() {
		
		return dao.listOne();
	}
// ��
	public int upOne(Worker_list worker_list) {
		return dao.upOne(worker_list);
	}

	public int upAll(List<Worker_list> user_list) {
	
		return dao.upAll(user_list);
	}
	@Override
	public Worker_list login(Worker_list worker_list) {
		// TODO Auto-generated method stub
		return dao.login(worker_list);
	}
}
