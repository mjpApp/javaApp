package com.seehope.service.impl;

import java.util.List;

import javax.annotation.Resource;

import org.springframework.stereotype.Service;

import com.seehope.dao.IAccount_listDao;
import com.seehope.pojo.Account_list;
import com.seehope.service.IAccountListService;
@Service("accountListService")
public class AccountListServiceImpl implements IAccountListService {
	
	
	@Resource
	private IAccount_listDao dao;
	
//	��
	public int insOne(Account_list account_list) {
		return dao.insOne(account_list);
	}
	public int insAll(List<Account_list> account_list) {
		return dao.insAll(account_list);
		
	}
//	ɾ
	public int delOne(Account_list account_list) {
		return dao.delOne(account_list);
		
	}

	public int delAll(List<Account_list> account_list) {
		return dao.delALL(account_list);
		
	}
//	��
	public Account_list getOne(Account_list account_list) {
		return dao.getOne(account_list);
		
	}
	
	public List<Account_list> list() {
		return dao.list();
	}
// ��ҳ
	public List<Account_list> listOne(int id) {
		
		return dao.listOne(id);
	}
// ��
	public int upOne(Account_list account_list) {
		return dao.upOne(account_list);
	}

	public int upAll(List<Account_list> account_list) {
	
		return dao.upAll(account_list);
	}
	@Override
	public int upCost(Account_list account_list) {
		// TODO Auto-generated method stub
		return dao.upCost(account_list);
	}
	@Override
	public Account_list getNum(Account_list account_list) {
		// TODO Auto-generated method stub
		return dao.getNum(account_list);
	}
}
