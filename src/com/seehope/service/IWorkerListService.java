package com.seehope.service;

import java.util.List;

import com.seehope.pojo.Worker_list;

public interface IWorkerListService {
	int insOne(Worker_list worker_list);
	int insAll(List<Worker_list> worker_list);
	
	int delOne(Worker_list worker_list);
	int delAll(List<Worker_list> worker_list);
	
	Worker_list getOne(Worker_list worker_list);
	List<Worker_list> list();
	List<Worker_list> listOne();
	
	int upOne(Worker_list worker_list);
	int upAll(List<Worker_list> worker_list);
	Worker_list login(Worker_list worker_list);
}
