package com.seehope.service;

import java.util.List;

import com.seehope.pojo.Prefer_list;

public interface IPreferListService {
	int insOne(Prefer_list Prefer_list);
	int insAll(List<Prefer_list> Prefer_list);
	
	int delOne(Prefer_list user_list);
	int delAll(List<Prefer_list> Prefer_list);
	
	Prefer_list getOne(Prefer_list Prefer_list);
	List<Prefer_list> list();
	List<Prefer_list> listOne();
	
	int upOne(Prefer_list Prefer_list);
	int upAll(List<Prefer_list> Prefer_list);

}
