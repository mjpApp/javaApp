package com.seehope.service;

import java.util.List;

import com.seehope.pojo.Supplier_list;

public interface ISupplierListService {
	int insOne(Supplier_list supplier_list);
	int insAll(List<Supplier_list> supplier_list);
	
	int delOne(Supplier_list supplier_list);
	int delAll(List<Supplier_list> supplier_list);
	
	Supplier_list getOne(Supplier_list supplier_list);
	List<Supplier_list> list();
	List<Supplier_list> listOne();
	
	int upOne(Supplier_list supplier_list);
	int upAll(List<Supplier_list> supplier_list);
}
